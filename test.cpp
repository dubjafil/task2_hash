#include <cstdio>
#include <cstring>
#include <openssl/evp.h>
#include <string>
#include <unistd.h>
#include <iostream>
#include <chrono>
#include <vector>

using namespace std;

std::string gen_random(const int len) {
    static const char alphanum[] =
            "0123456789"
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            "abcdefghijklmnopqrstuvwxyz";
    std::string tmp_s;
    tmp_s.reserve(len);

    for (int i = 0; i < len; ++i) {
        tmp_s += alphanum[rand() % (sizeof(alphanum) - 1)];
    }

    return tmp_s;
}

int main(int argc, char *argv[]) {
    int text_cnt = 1;
    int bits;
    int found = 0;
    if (argc != 2 || sscanf(argv[1], "%d", &bits) < 1) {
        printf("Hezci cislo pls");
        return 1;
    }
//    vector<pair<int, int>> to_plot;
    srand((unsigned) time(NULL) * getpid());
    string text = "a";
    char hashFunction[] = "sha512";  // zvolena hashovaci funkce ("sha1", "md5", ...)

    EVP_MD_CTX *ctx;  // struktura kontextu
    const EVP_MD *type; // typ pouzite hashovaci funkce
    unsigned char hash[EVP_MAX_MD_SIZE]; // char pole pro hash - 64 bytu (max pro sha 512)
    unsigned int length;  // vysledna delka hashe

    /* Inicializace OpenSSL hash funkci */
    OpenSSL_add_all_digests();
    /* Zjisteni, jaka hashovaci funkce ma byt pouzita */
    type = EVP_get_digestbyname(hashFunction);

    /* Pokud predchozi prirazeni vratilo -1, tak nebyla zadana spravne hashovaci funkce */
    if (!type) {
        printf("Hash %s neexistuje.\n", hashFunction);
        return 1;
    }
//    while (bits!=61) {
//        auto start = chrono::high_resolution_clock::now();
    ctx = EVP_MD_CTX_new(); // create context for hashing
    while (found != bits) {
        if (ctx == NULL)
            return 2;

        /* Hash the text */
        if (!EVP_DigestInit_ex(ctx, type, NULL)) // context setup for our hash type
            return 3;

        if (!EVP_DigestUpdate(ctx, text.c_str(), text.size())) // feed the message in
            return 4;

        if (!EVP_DigestFinal_ex(ctx, hash, &length)) // get the hash
            return 5;

        int binary[bits];
        for (int n = 0; n < bits; n++)
            binary[bits - 1 - n] = (hash[n / 8] >> n%8) & 1;
        for (int i = 0; i < bits; i++) {
            if (binary[i] == 0)
                found++;
        }
        if (found == bits)
            break;
        text = gen_random(1 + text_cnt / 62);
        text_cnt++;
        //printf("%s", text.c_str());
        found = 0;
    }
    EVP_MD_CTX_free(ctx); // destroy the context

    /* Vypsani vysledneho hashe */
    for (auto &it: text) {
        cout << hex << int(it);
    }
    cout << endl;
    for (unsigned int i = 0; i < length; i++) {
        printf("%02x", hash[i]);
    }
//        auto stop = chrono::high_resolution_clock::now();
//        printf("\n");
//        auto duration = chrono::duration_cast<chrono::microseconds>(stop - start);
//        to_plot.emplace_back(bits,duration.count());
//        bits += 1;
//        cout << bits;
//    }
//    for(auto &it: to_plot){
//        cout << it.second << endl;
//    }
    return 0;
}
